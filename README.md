# R and Geology

A list of (hopefully!) helpful resources on the use of R with different geological data types.


This list of different scripts and markdown documents is not designed to be complete, rather a look at how to approach problems with different tools - as a company [Solve Geosolutions](https://www.solvegeosolutions.com/) has always believed that while statistical analysis, coding and a working knowledge of machine learning should augment the geologists toolkit, they are not designed to replace the role of the geologist. 

Most of these examples here will be split into two different sections. Examples desgined to show one particular facet of an analysis (think, data merging of typical files you might get from either a geological survey or your database administrator) or more fleshed out workflows (merging, preprocessing and interpretation). 

A good fundamental understanding of geology and it's processes will most likely lead to a better result when paired with statistical techniques ([example 1](https://www.mdpi.com/2075-163X/10/1/61), [example 2](https://www.sciencedirect.com/science/article/pii/S0169136819302148), [example 3](https://link.springer.com/content/pdf/10.1007/s11004-020-09859-0.pdf)), the technique being used is secondary to putting the results in geological context.

## Data merging

